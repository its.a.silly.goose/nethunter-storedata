The Termux:Float add-on provides a floating terminal, allowing direct terminal access while running other apps.

NOTE: This is an add-on which requires that the main Termux app is installed to use.
