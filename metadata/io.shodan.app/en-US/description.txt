Using your API key, this application allows you to explore data gathered by Shodan.io in order to discover exposed services all over the Internet.
Search for specific terms or any popular queries and start browsing connected devices.

Privacy policy: https://github.com/PaulSec/Shodan.io-mobile-app/blob/master/privacy_policy.md


This app is built and signed by Kali NetHunter.
